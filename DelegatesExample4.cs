// Example-2 on Delegates
using System;  
 
    // Delegate Definition  
    public delegate int operation(int x);  
         
class Program  
{  
 
  static int cuber(int x)
  {
	return (x*x*x);
  }
  static int square(int y)
  {
	return (y*y);
  }
  
   static void Main(string[] args)  
   { 
	Console.WriteLine("Enter the Number you want to square and cube");
	int n = Convert.ToInt32(Console.ReadLine());
	operation obj1=new operation(Program.cuber);
	Console.WriteLine("Cube :: {0}",obj1(n));
	operation obj2=new operation(Program.square);
	Console.WriteLine("Square ::  {0}",obj2(n));
	}
  
 }