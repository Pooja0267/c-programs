using System;
public class Base
{
	public Base()
	{
		Console.WriteLine("Base class Constructor.");
	}
}
public class Derived : Base
{
	public Derived()
	{
		Console.WriteLine("Derived class Constructor.");
	}
	
	static void Main(string[] args)
	{
		Base ob1 = new Base();
		
		Derived ob2 = new Derived();
	}
}

