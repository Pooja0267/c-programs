//Demonstrate the use of is and as.
using System;
interface IConvertible
{
	string ConvertToCSharp(string str1);
	string ConvertToVB(string str2);
}

interface ICodeChecker: IConvertible
{
	bool CodeCheckSyntax(string str3, string str4);
}

class ProgramConverter : IConvertible
{
	
	public string ConvertToCSharp(string s1)
	{
		Console.WriteLine("Your code: "+ s1 + " will be converted to C# here.");
		return "Done to C#";
	}
	
	
	public string ConvertToVB(string s2)
	{
		Console.WriteLine("Your code: "+ s2 + " will be converted to VB here.");
		return "Done to VB";
	}
}

class ProgramHelper: ProgramConverter,ICodeChecker
{

public bool CodeCheckSyntax(string s3, string s4)
	{
		if(s4== "C#")
		return true;
	
		else if(s4=="VB")
		return true;
	
		else
		return false;
	}
	
	static void Main(string[] args)
	{
		ProgramHelper Objects[] ;
		//= new ProgramHelper();
		for(int i=0;i<=5;i++)
		{
			Objects[i] = new ProgramHelper();
			
			Console.WriteLine("Enter code to convert to CSharp");
			string codeCsharp = Objects[i].ConvertToCSharp(Console.ReadLine());
			Console.WriteLine(codeCsharp);
			
			bool response1 = Objects[i].CodeCheckSyntax(codeCsharp,"C#");
			Console.WriteLine(response1);
		}
	}

}


