//Program 3: Write a program to Find the frequency of the word “and” [if exists] in a given input string and replace all "and" with "AND".
using System;
using System.Text;

class Program
{
	static void Main(string[] args) 
	{
		Console.WriteLine("Enter a Sentence");
		string str = Console.ReadLine();
		string s=str;
		int count=0;
		
		const char Space = ' ';
		const char Comma = ',';
		
		char [] delimiters= new char[]{Space,Comma};
		string [] arr = str.Split(delimiters);
		foreach (string element in arr)
		{
			if(element == "and")
			{
				count++;

				s = str.Replace("and", "AND");
			}
		}
		
		Console.WriteLine("Frequency= {0}", count); 
		
		Console.WriteLine(s);
		
	}
}