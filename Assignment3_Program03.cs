/* Consider two Dictionaries.First one containing the product name and product category code as key and value respectively. 
Second one contains the product name and price. Write a method in c# which accepts the two dictionaries , price hike rate and
 the product category and updates the prices of the product in the entered category by the hike rate. print the 2nd dictionary with updated price.
example
Input1 :{“lux”:”soap”,”colgate”:”paste”, ”pears”:”soap”,”sony”:”electronics”,”samsung”:”electronics”}
Input 2:{“lux”:1000,”colgate”:500,”pears”:2000,”sony”:100,” samsung”,600}
Input 3: 10
Input4: “electronics”
Output1: :{“lux”:1000,”colgate”:500,”pears”:2000,”sony”:110,” samsung”,660} */

using System;
using System.Collections;
using System.Collections.Generic;

public class Program
{
	static void Main(string[] args)
	{
		Dictionary<string, string> dictionary1 = new Dictionary<string, string>();
		Dictionary<string, int> dictionary2 = new Dictionary<string, int>();
		
		string key1 = "";
		string key2 = "" ;
		
		string value1= "";
		int value2 = 0;
		
		Console.WriteLine("Enter the number of products you want to enter");
		int products = Convert.ToInt32(Console.ReadLine());
		Console.WriteLine("\n");
		
		Console.WriteLine("Enter the Product Name and Product Category as key and value respectively.");
		Console.WriteLine("\n");
		
		for(int i=1;i<=products; i++)
		{
			Console.Write("Product Name(Key) : ");
			key1 = Console.ReadLine();
			Console.Write("Product Category(Value) : ");
			value1 = Console.ReadLine();
			Console.WriteLine("\n");
			dictionary1.Add(key1, value1);
		}
		
		
		Console.WriteLine("Enter the Product Name and Price as key and value respectively.");
		Console.WriteLine("\n");
		
		for(int j=1;j<=products; j++)
		{
			Console.Write("Product Name(Key) : ");
			key2 = Console.ReadLine();
			Console.Write("Product Price(Value) : ");
			value2 = Convert.ToInt32(Console.ReadLine());
			Console.WriteLine("\n");
			dictionary2.Add(key2, value2);
		}
		
		Console.WriteLine("Enter the hike in price you want.");
		int hike = Convert.ToInt32(Console.ReadLine());
		
		Console.WriteLine("Enter the the product category where you want the hike.");
		string category = Console.ReadLine();
		
		// Store keys in a List for dictionary1.
		List<string> keys = new List<string>(dictionary1.Keys);
		
		// Store values in a List for dictionary1.
		List<string> values = new List<string>(dictionary1.Values);
		
		// Store keys in a List for dictionary2.
		List<string> keys2 = new List<string>(dictionary2.Keys);
		
		// Store values in a List for dictionary2.
		List<int> values2 = new List<int>(dictionary2.Values);
		
		
		foreach (KeyValuePair<string, string> pair in dictionary1)
		{
			if(pair.Value==category)
			{
				string tempKey = pair.Key;
				for(int m=1; m<=keys2.Count; m++)
				{
					int temp1 = (dictionary2[tempKey]/100);
					int temp2 = 100+hike;
					int new_price = temp1*temp2;
					dictionary2[tempKey] = new_price;
					
					
				}
			}
		}
		
		/*for(int k=1;k<=keys.Count; k++)
		{
			if(values.Contains(category))
			{
				string tempKey = dictionary1.Key;
				//Console.WriteLine(dictionary1.key[string category]);
				for(int m=1; m<=keys2.Count; m++)
				{
					int new_price = (dictionary2[tempKey]*(100+hike))/100;
					dictionary2[tempKey] = new_price;
					
					
				}
			}
		} */
		
		foreach (KeyValuePair<string, int> pair in dictionary2)
		{
			Console.WriteLine(pair.Key.ToString ()+ "  -  "  + pair.Value.ToString () );
		}
		
		
		
		
		
	}
}