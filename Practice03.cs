//Create a class containing a method to create the mirror image of a String. The method should return the two Strings separated with a pipe(|) symbol .
using System;
class Program
{
	string s1=string.Empty;
	public string getImage(string s)
	{
		for(int i=s.Length-1;i>=0;i--)
		{
			s1 = s1+s[i];
		}
		return s1;
	}


}
class Tester
{
	static void Main(string[] args)
	{
		Console.WriteLine("Enter the string you want to Reverse");
		string str = Console.ReadLine();
		Program obj = new Program();
		string s2 = obj.getImage(str);
		Console.WriteLine(str+"|"+s2);
	}
}