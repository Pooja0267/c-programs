// Create a method to check if a number is an increasing number.
//A number is said to be an increasing number if no digit is exceeded by the digit to its left.

using System;
public class Program
{
	
	public bool checkNumber(int n)
	{
		string s= n.ToString();
		bool r=false;
		for(int i=0;i<s.Length-1;i++)
		{
			if(s[i]<=s[i+1])
			{
				r= true;
			}
			else
			{
				r= false;
				break;
			}
			
			
		}
		return r;
		
	}


}
public class Tester
{
	static void Main(string[] args)
	{
		Console.WriteLine("Enter the number you want to check.");
		int num = Convert.ToInt32(Console.ReadLine());
		Program obj = new Program();
		bool reply = obj.checkNumber(num);
		if(reply == true)
		{
			Console.WriteLine("It is an increasing number");
		}
		else
		{
			Console.WriteLine("It is not an increasing number");
		}
	}
}