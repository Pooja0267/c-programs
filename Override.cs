using System;
public class Telephone
{
	protected string phonetype;
	public virtual void Ring()
	{
		Console.WriteLine("Ringing the Telephone.");
	}
}

class ElectronicTelephone : Telephone
{
	ElectronicTelephone()
	{
		string phonetype= "Digital" ;
		Console.WriteLine("Phone Type: {0}", phonetype);
	}
	public override void Ring()
	{
		//base.Ring();
		Console.WriteLine("Ringing the ElectronicTelephone.");
	}
	
	static void Main(string[] args)
	{
		ElectronicTelephone ob = new ElectronicTelephone();
		ob.Ring();
	}

}