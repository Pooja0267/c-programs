/*Exercise 14-1. Create an abstract Animal class that has private members weight and name, and abstract methods Speak(), Move(), and ToString(). 
Derive from Animal a Cat class and a Dog class that override the methods appropriately. Create an Animal array, populate it with Dogs and Cats, 
and then call each member’s overridden virtual methods.*/

using System;
using System.Collections;

abstract class Animal
{
	//private int weight;
	//private string name;
	
	/*public int weight
	{
		get
		{
			return weight;
		}
		set(int value)
		{
			weight = value;
		}		
	}
	
	public string name
	{
		get
		{
			return name;
		}
		set(string value)
		{
			name = value;
		}		
	}*/
	
	abstract public void Speak();
	abstract public void Move();
	abstract public void ToString1();
}

class Cat : Animal
{
	string n=string.Empty;
	int w = 0;
	
	public override void Speak()
	{
		Console.WriteLine("A cat purrs.");
	}
	public override void Move()
	{
		Console.WriteLine("A cat Jumps.");
	}
	public override void ToString1()
	{
		Console.WriteLine("Enter the cat's name");
		n = Console.ReadLine();
		
		Console.WriteLine("Enter the cat's weight");
		w = Convert.ToInt32(Console.ReadLine());
		
	}
}

class Dog: Animal
{
	string n= string.Empty;
	int w= 0;
	
	public override void Speak()
	{
		Console.WriteLine("A dog barks.");
	}
	public override void Move()
	{
		Console.WriteLine("A dog runs.");
	}
	public override void ToString1()
	{
		Console.WriteLine("Enter the dog's name");
		n = Console.ReadLine();
		
		Console.WriteLine("Enter the dog's weight");
		w = Convert.ToInt32(Console.ReadLine());
	}
}
class Program
{
	static void Main(string[] args)
	{
		Animal[] ar=new Animal[2];
		//Cat obj1 = new Cat();
		//Dog obj2 = new Dog();
		ar[0]=new Cat();
		ar[1]=new Dog();
		
		ar[0].ToString1();
		ar[0].Speak();
		ar[0].Move();
		//obj1.Speak();
		//obj1.Move();
		
		
		ar[1].ToString1();
		ar[1].Speak();
		ar[1].Move();
		//obj2.Speak();
		//obj2.Move()
		
	}
}