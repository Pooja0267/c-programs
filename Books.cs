using System;

public class Book
{
	public void displayBook(string author, string publisher, string title, string isbn)
	{
			Console.WriteLine("Author: {0}", author + "   ");
			Console.WriteLine("Publisher: {0}",publisher + "   ");
			Console.WriteLine("Title: {0}",title + "   ");
			Console.WriteLine("ISBN: {0}",isbn + "   ");
			//Console.WriteLine("/n");
			
	}
}
class Program : Book
{
	static void Main(string[] args)
	{
		Book ob1 = new Program();
		ob1.displayBook("Pooja","ABC","I Too Had A Love Story","9780596527433");
		Book ob2 = new Program();
		ob2.displayBook("Ashia","XYZ","Concepts of C#","9780594643653");
		
		
	}

}

