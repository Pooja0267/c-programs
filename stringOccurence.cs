//Displaying only unique characters.
using System;
using System.Linq;
public class Program
{
public static void Check(string n)
	{ 
		string str = new String(n.Distinct().ToArray());
		Console.Write(str);
		
	}

	public static void Main(string[] args)
	{
		Console.WriteLine("Enter a name");
		string name= Console.ReadLine().ToLower();
		Check(name);
		
	}
}