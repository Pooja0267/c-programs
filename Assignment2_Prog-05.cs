//Find two strings are anagram or not.

using System;
class Program
{
	static void Anagram(string s1, string s2)
	{
		if(s1.Length == s2.Length)
		{
			Char[] n1 = s1.ToCharArray();
			Char[] n2 = s2.ToCharArray();
			Array.Sort(n1);
			Array.Sort(n2);
			string string1 = new string(n1);
			string string2 = new string(n2);
			
			if(string1== string2)
			{
				Console.WriteLine("The strings are Anagram");
			}
			else
			{
				Console.WriteLine("The strings are not Anagram");
			}
		}
		
		else
		{
			Console.WriteLine("The strings are not Anagram");
		}
	}
	
	static void Main(string[] args)
	{
		Console.WriteLine("Enter two strings");
		string str1 = Console.ReadLine().ToLower();
		string str2 = Console.ReadLine().ToLower();
		
		Anagram(str1,str2);
		
		
	}
}
