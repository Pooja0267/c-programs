using System;

class Program
{
	static void Main(string[] args)
	{
		try
		{
			Console.WriteLine("Enter two numbers");
			int num1= Convert.ToInt32(Console.ReadLine());
			int num2= Convert.ToInt32(Console.ReadLine());
			
			if (num1 == 0)
			{
				throw new DivideByZeroException( );
			}
			if (num2 == 0)
			{
				throw new ArithmeticException( );
			}
			int result = num2/num1;
			Console.WriteLine("Division Successful!!!"+"Result={0}",result);
		}
		
		
		// most specific exception type first
		catch (DivideByZeroException e)
		{
			Console.WriteLine("DivideByZeroException caught!");
			Console.WriteLine("\nDivideByZeroException! Msg: {0}",e.Message);
		 }
		
		catch (ArithmeticException)
		{
			Console.WriteLine("ArithmeticException caught!");
		}
		// generic exception type last
		catch
		{
			Console.WriteLine("Unknown exception caught");
		}
	}
}