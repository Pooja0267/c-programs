//Program 1: Write a function which will take a string as an input and will capitalize every alternate words in it and print them.
using System;

public class Program
{
	static void Main(string[] args)
	{
		Console.WriteLine("Enter a String");
		string str = Console.ReadLine();
		
		for(int i=0;i<str.Length;i++)
		{
			if(i%2==0)
			{
				Console.Write(str[i]);
			}
			else
			{
				Console.Write(str[i].ToString().ToUpper());
			}
		}
	}
}