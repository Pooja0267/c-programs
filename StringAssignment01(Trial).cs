//Program 1: Write a function which will take a string as an input and will capitalize every alternate words in it and print them.
using System;
class Program
{
	static void Main(string[] args) 
	{
		Console.WriteLine("Enter a Sentence");
		string str = Console.ReadLine();
		
		
		const char Space = ' ';
		const char Comma = ',';
		
		char [] delimiters= new char[]{Space,Comma};
		
		string [] arr = str.Split(delimiters);
		
		
			for(int i=0;i<str.Length;i++)
			{
				if(i%2==0)
				{
					Console.Write(str[i]);
				}
				else
				{
					Console.Write(str[i].ToString().ToUpper());
				}
			}
		
	}
}