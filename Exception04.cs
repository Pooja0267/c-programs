using System;

public class MyCustomException : System.Exception
{
	public MyCustomException(string Message):base(Message)
	{
		
	}
}


class Tester
{
	static void Main(string[] args)
	{
		int n=0;
		try
		{
			Console.WriteLine("Enter your Name:");
			string name = Console.ReadLine();
			
			int.TryParse(name,out n);
			if (String.IsNullOrEmpty(name) )
			{
				throw new NullReferenceException();
			}
			
			if (n!=0)
			{
				MyCustomException ex = new MyCustomException("Invalid Name");
				throw ex;
			}
			
			Console.WriteLine(name);
		}
		
		catch(MyCustomException ex)
		{
			Console.WriteLine("Exception Caught");
			Console.WriteLine(ex.Message);
		}
		
		catch(NullReferenceException e)
        {
            Console.WriteLine("Exception Caught");
			Console.WriteLine(e.Message);
		}
	}
}