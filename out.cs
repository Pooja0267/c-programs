using System;
class Program
{
	static int Add(out int x,out int y)
	{
		x=10;
		y=20;
		Console.WriteLine("Value 1:  "+x);
		Console.WriteLine("Value 2:  "+y);
		return x+y ;
	}
	
	static int Diff(out int x,out int y)
	{
		x=20;
		y=50;
		Console.WriteLine("Value 1:  "+x);
		Console.WriteLine("Value 2:  "+y);
		return y-x ;
	}
	
	static int Mul(out int x,out int y)
	{
		x=2;
		y=5;
		Console.WriteLine("Value 1:  "+x);
		Console.WriteLine("Value 2:  "+y);
		return x*y;
	}
	
	static int Div(out int x,out int y)
	{
		x=5;
		y=50;
		Console.WriteLine("Value 1:  "+x);
		Console.WriteLine("Value 2:  "+y);
		return y/x ;
	}
	
	static int Mod(out int x,out int y)
	{
		x=6;
		y=50;
		Console.WriteLine("Value 1:  "+x);
		Console.WriteLine("Value 2:  "+y);
		return y%x ;
	}
	static void Main(string[] args)
	{
		int sum;
		int a;
		int b;
		int difference;
		int product;
		int quotient;
		int modulus;
		
		
		sum = Add(out a, out b);
		Console.WriteLine("Sum of{0} + {1} is ={2}",a,b,sum);
		Console.WriteLine("\n");
		
		difference =  Diff(out a, out b);
		Console.WriteLine("Difference of{0} - {1} is ={2}",b,a,difference);
		Console.WriteLine("");
		
		product = Mul(out a, out b);
		Console.WriteLine("Product of{0}*{1} is ={2}",a,b,product);
		Console.WriteLine("");
		
		quotient= Div(out a, out b);
		Console.WriteLine("Quotient of{0}/{1} is ={2}",b,a,quotient);
		Console.WriteLine("");
		
		modulus = Mod(out a, out b);
		Console.WriteLine("Remainder of{0}% {1} is ={2}",b,a,modulus);
		
		
	}
}