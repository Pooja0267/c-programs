//Create a method to check if a number is a power of two or not

using System;
public class Program
{
	
	public bool checkNumber(int n)
	{
		int p=1;
		bool r= false;
		for(int i=1;i<50;i++)
		{
			p = p*2;
			Console.WriteLine(p);
			if(p==n)
			{
				r= true;
				break;
			}
			
		}
		return r;
	}


}
public class Tester
{
	static void Main(string[] args)
	{
		Console.WriteLine("Enter the number you want to check.");
		int num = Convert.ToInt32(Console.ReadLine());
		Program obj = new Program();
		bool reply = obj.checkNumber(num);
		if(reply == true)
		{
			Console.WriteLine("The given number is power of 2");
		}
		else
		{
			Console.WriteLine("The given number is not a power of 2");
		}
	}
}