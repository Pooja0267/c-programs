//Interface ex-01 & 02
using System;
interface IConvertible
{
	string ConvertToCSharp(string str1);
	string ConvertToVB(string str2);
}


class ProgramHelper: IConvertible
{
	public string ConvertToCSharp(string s1)
	{
		Console.WriteLine("Your code: "+ s1 + " will be converted to C# here.");
		return "Done to C#";
	}
	
	
	public string ConvertToVB(string s2)
	{
		Console.WriteLine("Your code: "+ s2 + " will be converted to VB here.");
		return "Done to VB";
	}
	
	
	static void Main(string[] args)
	{
		ProgramHelper obj = new ProgramHelper();
		
		Console.WriteLine("Enter code to convert to CSharp");
		string codeCsharp = obj.ConvertToCSharp(Console.ReadLine());
		Console.WriteLine(codeCsharp);
		
		Console.WriteLine("Enter code to convert to VB");
		string codeVB = obj.ConvertToVB(Console.ReadLine());
		Console.WriteLine(codeVB);
	}
}