using System;
class Program
{
	static int Add(ref int x,ref int y)
	{
		return x+y ;
	}
	
	static int Diff(ref int x,ref int y)
	{
		return y-x ;
	}
	
	static int Mul(ref int x,ref int y)
	{
		return x*y ;
	}
	
	static int Div(ref int x,ref int y)
	{
		return y/x ;
	}
	
	static int Mod(ref int x,ref int y)
	{
		return y%x;
	}
	static void Main(string[] args)
	{
		int sum= 0;
		int a = 10;
		int b = 20;
		
		sum = Add(ref a, ref b);
		Console.WriteLine("Sum is "+sum);
		
		Console.WriteLine("Difference is "+ Diff(ref a, ref b));
		
		Console.WriteLine("Product is "+ Mul(ref a, ref b));
		
		Console.WriteLine("Quotient is "+ Div(ref a, ref b));
		
		Console.WriteLine("Remainder is "+ Mod(ref a, ref b));
		
		
	}
}