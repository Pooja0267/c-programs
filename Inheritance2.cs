using System;
public abstract class Base
{
	public string name;
	public Base()
	{
		Console.WriteLine("I am base class Constructor.");
	}
	
	abstract public void Phone();
	public virtual void Names()
	{
		Console.WriteLine("Enter the name");
		 name = Console.ReadLine();
	}

}
public class Program : Base
{
	string age;
	string phone;
	string number;
	public Program()
	{
		Console.WriteLine("I am Derived class Constructor.");
	}
	public override void Names()
	{
		base.Names();
		Console.WriteLine("Enter the age");
		age = Console.ReadLine();
		
	}
	public override void Phone()
	{
		Console.WriteLine("Enter your Phone No.");
		number = Console.ReadLine();
		if(number.Length<10 || number.Length>10)
		{
			Console.WriteLine("It is not a valid phone no.");
			
		}
		else
		{
			phone = number;
		}
	}
	
	public void Display()
	{
		Console.WriteLine("\n");
		Console.WriteLine("Name:{0}", name);
		Console.WriteLine("Age: {0} years", age);
		Console.WriteLine("Phone No.: {0}", phone);
	}
	public static void Main(string[] args)
	{
		Program p= new Program();
		p.Names();
		p.Phone();
		p.Display();
	}
}