//Write a method to generate the nth Fibonacci number.
using System;
class Program
{
	static void Fibo(int n)
	{
		int first=0;
		int second=1;
		int third=0;
		
		//Console.WriteLine(first+"\t" );
		//Console.WriteLine(second+"\t" );
		for(int i=0;i<n-2;i++) 
		{
			third = first+second;
			first = second;
			second= third;
			
			//Console.WriteLine(third+"\t" );
		}
		
		Console.WriteLine("The nth Fibonacci number is:{0}" , third);
	}
	static void Main(string[] args)
	{
		Console.WriteLine("Enter a number to see the nth Fibonacci number");
		int num= Convert.ToInt32(Console.ReadLine());
		Fibo(num);
	}
}
