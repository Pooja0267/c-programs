using System;
public abstract class Telephone
{
	protected string phonetype;
	public virtual void Ring()
	{
		Console.WriteLine("Ringing the Telephone.");
	}
}

public class ElectronicTelephone : Telephone
{
	public ElectronicTelephone()
	{
		phonetype= "Digital" ;
		Console.WriteLine("Phone Type: {0}", phonetype);
	}
	public override void Ring()
	{
		Console.WriteLine("Ringing the ElectronicTelephone.");
	}
	
}

class Talkingphone : Telephone
{
	Talkingphone()
	{
		phonetype= "Talking Phone" ;
		Console.WriteLine("Phone Type: {0}", phonetype);
	}
	public override void Ring()
	{
		Console.WriteLine("Ringing the Talking Phone.");
	}
	
	public static void Main(string[] args)
	{
		ElectronicTelephone ob = new ElectronicTelephone();
		ob.Ring();
		
		Talkingphone ob2 = new Talkingphone();
		ob2.Ring();
	}

}