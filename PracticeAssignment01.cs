//To find the missing number in integer array of 1 to 100? 
using System;

class Program
{
	static void Main(string[] args)
	{
		try
		{
			Console.WriteLine("Enter the size of Array.");
			int size = Convert.ToInt32(Console.ReadLine()) ;
			
			int[] myArray = new int[size];
			//int[] arr = new int[10];
			int sum = size*(size+1)/2;
			int tempSum=0;

			Console.WriteLine("Enter the Array elements");
			for(int i=0; i<size-1; i++)
			{
				myArray[i]=Convert.ToInt32(Console.ReadLine()) ;
				tempSum = tempSum+myArray[i];
			}
			
			/*Console.WriteLine("Enter the Array elements");
			for(int j=0; j<10; j++)
			{
				arr[j]=Convert.ToInt32(Console.ReadLine()) ;
			}
			//Finding the sum of elements of the given array
			for(int j=0; j<10; j++)
			{
				tempSum = tempSum+arr[j];
			}*/
			
			if(sum==tempSum)
			{
				Console.WriteLine("No number is missing");
			}
			else
			{
				int diff = sum-tempSum;
				Console.WriteLine("The missing  number is: {0}", diff);
				
			}
		
		}
		
		catch (IndexOutOfRangeException e)  
        {
            Console.WriteLine(e.Message);
        }
		
	}
}