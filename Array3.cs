using System;
public class Tester
{
	static void Main( )
	{
		const int rows = 4;
		const int columns = 3;
		// declare a 4x3 integer array
		//The brackets in the int[,] declaration indicate that the type is an array of integers,and the comma indicates the array has 2D (two commas would 
		//indicate 3D, and so on).
		
		int[,] rectangularArray = new int[rows, columns];
		//int[,] rectangularArray = { {0,1,2}, {3,4,5}, {6,7,8}, {9,10,11} };
		// populate the array
		for (int i = 0; i < rows; i++)
		{
			for (int j = 0; j < columns; j++)
			{
				rectangularArray[i, j] = i + j;
			}
		}
		// Output the array
		for (int i = 0; i < rows; i++)
		{
			for (int j = 0; j < columns; j++)
			{
				Console.WriteLine("rectangularArray[{0},{1}] = {2}",i, j, rectangularArray[i, j]);
			}
		}
	}
}