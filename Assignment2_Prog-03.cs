//Stack & Queue
using System;
public class Stack
{
	int[] arr = new int[10];
	static int i=0;
	
	// Push to Stack
	public void Push(int p)
	{
		
		arr[i]= p;
		i++;
		if(i>9)
		{
			Console.WriteLine("Array out of Range");
		}
	}
	
	// Display Top
	public void DisplayTop()
	{
		if(i>0)
		{
		Console.WriteLine("Top Element: {0}", arr[i-1]);
		}
		else
		{
			Console.WriteLine("Stack is Empty");
		}
	}
	
	// Pop Top Element
	public void Pop()
	{
		i--;
	}
	
	//Display all array elements
	public void DisplayAll()
	{
		Console.WriteLine("The Stack elements are:");
		for(int j=i-1;j>=0;j--)
		{
			Console.WriteLine("Stack[{0}]:{1}",j,arr[j]);
		}
	}
	
}
public class Program
{
	static void Main(string[] args)
	{
		Stack s = new Stack();
		Start:
		Console.WriteLine("\nStack MENU(size:10)");
        Console.WriteLine("1. Add an element");
        Console.WriteLine("2. See the Top element.");
        Console.WriteLine("3. Remove top element.");
        Console.WriteLine("4. Display stack elements.");
        Console.Write("Select your choice: ");
		int input = Convert.ToInt32(Console.ReadLine());
		
		switch(input)
		{
		case 1: 
			 Console.WriteLine("Enter an Element to Push: ");
			 int a = Convert.ToInt32(Console.ReadLine());
			 s.Push(a);
			 goto Start;
		case 2: 
				s.DisplayTop();
				goto Start;
		case 3: 
				s.Pop();
				goto Start;
		case 4: 
				s.DisplayAll();
				goto Start;		
		}
	}
}