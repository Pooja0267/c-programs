/* Modify the example in Exercise 16-1 to handle two specific errors: the IndexOutOfRangeException, which is used when the user enters a number that’s not
valid for the array, and the FormatException, which is used when the entered value doesn’t match the expected format—in this case, if the user enters something 
that isn’t a number. Leave the existing handler as a default.*/

using System;

class Program
{
	static void Main(string[] args)
	{
		//int count=0;
		//int n=0;
		
		try
		{
			Console.WriteLine("Enter the size of the array");
			int size = Convert.ToInt32(Console.ReadLine());
			
			int[] myArray = new int[size];
			
			for(int i=0;i<size;i++)
			{
				string temp = Console.ReadLine();
				int value=0;
				if (int.TryParse(temp, out value))
				{
					myArray[i]= value;
				}
				else
				{
					FormatException ex = new FormatException("Invalid Format.Please enter a number");
					throw ex;
				}
				
			}
			
			/*for(int i=0;i<size;i++)
			{
				int.TryParse(myArray[i],out n);
				if(n!=0)
				{
					FormatException ex = new FormatException("Invalid Format.Please enter a number");
					throw ex;
				}
			}*/
			
		
		}
		catch (IndexOutOfRangeException e)  
        {
            Console.WriteLine(e.Message);
        }
		
		catch(FormatException ex)
		{
			Console.WriteLine("Exception Caught");
			Console.WriteLine(ex.Message);
		}
	}
}
