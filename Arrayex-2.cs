using System;
class Program
{
	static void Main(string[] args)
	{
		//Console.WriteLine("Enter the Size of Array");
		//int size = Convert.ToInt32(Console.ReadLine());
		
		int[] arr = new int[10];
		
		Console.WriteLine("Enter the Array Elements.");
		for(int i = 0;i<=9;i++)
		{
			arr[i]= Convert.ToInt32(Console.ReadLine());
			//Console.WriteLine(arr[i]);
		}
		
		Console.WriteLine("Un-Sorted Array");
		for(int j=0; j<=9;j++)
		{
			Console.WriteLine("arr[{0}] = {1}",j,arr[j]);
		}
		
		Array.Sort(arr);
		Console.WriteLine("Sorted Array");
		for(int j=0; j<=9;j++)
		{
			Console.WriteLine("arr[{0}] = {1}",j,arr[j]);
		}

	}
}