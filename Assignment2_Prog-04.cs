//Write a function to reverse words in  a sentence.
using System;
class Program
{
	static void Reverse(string s)
	{
		Char[] n = s.ToCharArray();
		Array.Reverse(n);
		string string1 = new string(n);
		Console.Write(string1 +"  ");
	}
	static void Main(string[] args) 
	{
		Console.WriteLine("Enter a Sentence");
		string str = Console.ReadLine();
		
		
		const char Space = ' ';
		const char Comma = ',';
		
		char [] delimiters= new char[]{Space,Comma,'#', '@', '!', '*', '&', '+', '-'};
		
		string [] arr = str.Split(delimiters);
		foreach (string element in arr)
		{
			Reverse(element);
		}
	}
}

 