/*A school offers medals to the students of tenth based on the following criteria
If(Marks>=90) : Gold
If(Marks between 80 and 90) : Silver
If(Marks between 70 and 80) : Bronze
Note: Marks between 80 and 90 means  marks>=80 and marks<90
Write a function which accepts the marks of students as a Hashmap and return the details of the students eligible for the medals along with type of medal.

The input hashmap contains the student registration number as key and mark as Value.
The output hashmap should contain the student registration number as key and the medal type as Value.
Method Name: getStudents
Method Description: Generate the list of students eligible for scholarship
Argument: Hashmap
Return Type: Hashmap
Logic: The method should return the details of the students eligible for the medals along with the medal type.*/



using System;
using System.Collections;
class Program
{
	public static Hashtable getStudents(Hashtable ht)
	{
		Hashtable h1 = new Hashtable();
		foreach(DictionaryEntry entry in ht)
		{
			if((int)entry.Value >=90)
			{
				h1.Add(entry.Key,"Gold");
			}
			if((int)entry.Value >=80 && (int)entry.Value<90)
			{
				h1.Add(entry.Key,"Silver");
			}
			if((int)entry.Value >=70 && (int)entry.Value < 80)
			{
				h1.Add(entry.Key,"Bronze");
			}
		}
		return h1;
	}
}

class Tester
{
	static void Main()
    {
		int regd;
		int marks;
		Hashtable hashtable = new Hashtable();
		Hashtable h2 = new Hashtable();
		Console.WriteLine("Enter the number of students");
		int n = Convert.ToInt32(Console.ReadLine());
		for(int i=0;i<n;i++)
		{
			Console.WriteLine("Enter the registration number.");
			regd = Convert.ToInt32(Console.ReadLine());
			
			Console.WriteLine("Enter the marks.");
			marks = Convert.ToInt32(Console.ReadLine());
			
			hashtable.Add(regd,marks);
		}
		
		
			Program obj = new Program();
			h2 = obj.getStudents(hashtable);
		foreach(DictionaryEntry newEntry in h2)
		{
			Console.WriteLine("{0}, {1}", newEntry.Key, newEntry.Value);
		}
	}
}