/*Create a Customer class having following properties.
1.CustomerId
2.FirstName
3.LastName
4.Age
5.Address

Create a List<T> and which will be used for below operations.

1.Adding a customer to the custom generic collection.
2.Removing a customer from the custom generic collection.
3.Searching a customer in the custom generic collection.
4.Listing all the customers in the custom generic collection.
5.Sorting the customer collection based on custom order.


Create a console application that will support these above operations.
Users can input various details of a customer and can use the add,remove,search,sort and list
functionalities.*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

public class Customer : IComparable 
	{
		//protected Guid? _UniqueId;
        //local member variable which stores the object's UniqueId

		public static int val=0;
        protected string _FirstName = "";
        protected string _LastName = "";
		protected int _CustomerId = 0 ; 
		protected int _age = 0;
		protected string _address = "";
		
		 //Default constructor
        public Customer()
        {
            
            //create a new unique id for this business object
			// _UniqueId = Guid.NewGuid();
       
        }
		
		//Paramaterized constructor for immediate instantiation
        public Customer(string first, string last, int id, int age , string add)
        {
            _FirstName = first;
            _LastName = last;
			_CustomerId = id;
			_age = age; 
			_address = add ;
			
        }

       //UniqueId property for every business object
        /*public Guid? UniqueId
        {
            get
            {
                return _UniqueId;
            }
            set
            {
                _UniqueId = value;
            }
        } */

        //Customer' First Name 
        public string FirstName
        {
            get
            {
                return _FirstName;
            }
            set
            {
                _FirstName = value;
            }
        }

        //Customer's Last Name
        public string LastName
        {
            get
            {
                return _LastName;
            }
            set
            {
                _LastName = value;
            }
        }
		
		//Customer's CustomerID
		public int CustomerID
		{
			get
			{
				return _CustomerId;
			}
			set
			{
				_CustomerId = value;
			}
		}
		
		//Customer's age
		public int Age
		{
			get
			{
				return _age;
			}
			set
			{
				_age = value;
			}
		}
		
		//Customer's address
		public string Address
		{
			get
			{
				return _address;
			}
			set
			{
				_address = value;
			}
		}
		
		public int CompareTo(object obj)
		{
			Customer other = obj as Customer;
			switch(val)
			{
				
				case 1:
					//Customer other = obj as Customer;
					if ( this.CustomerID > other.CustomerID ) return 1;
					else if ( this.CustomerID < other.CustomerID ) return -1;
					else return 0;
					
				case 2:
					
					int response1 =string.Compare(FirstName,other.FirstName);
					return response1;
			
					
				case 3:
					
					int response2 =string.Compare(LastName,other.LastName);
					return response2;
					
				case 4:
					if ( this.Age > other.Age ) return 1;
					else if ( this.Age < other.Age ) return -1;
					else return 0;
				
				default:
					if ( this.CustomerID > other.CustomerID ) return 1;
					else if ( this.CustomerID < other.CustomerID ) return -1;
					else return 0;
			}
		
		}
		
		protected List<Customer> _newCustomerList = new List<Customer>();
		//Sort
		public void Sort()
		{
			
			_newCustomerList.Sort();
		}
		
		
		
	
	}
	
	public class CustomerComparer : IComparer<Customer> 
		{
			public int Compare(Customer x,Customer y)
			{
				// Invoking a CompareTo method to compare each item x of the collection with the current item y specified
				return x.CompareTo(y);
			}
		}
	

internal class Program
    {
       private static void Main(string[] args)
        {
			List<Customer> customerList = new List<Customer>();
			
			Start:
			Console.WriteLine("Enter your Choice");
			Console.WriteLine("1. To Add a customer to the custom generic collection.");
			Console.WriteLine("2. To Display all customers in the custom generic collection.");
			Console.WriteLine("3. To Remove a customer from the custom generic collection.");
			Console.WriteLine("4. To Search a customer in the custom generic collection.");
			Console.WriteLine("5. To Sort the customer collection.");
			
			int choice = Convert.ToInt32(Console.ReadLine());
			Console.WriteLine("\n");
			
			switch(choice)
			{
				case 1:
					Console.WriteLine("Enter the First Name of the Customer");
					string fName = Console.ReadLine();
					Console.WriteLine("\n");
					
					Console.WriteLine("Enter the Last Name of the Customer");
					string lName = Console.ReadLine();
					Console.WriteLine("\n");
					
					Console.WriteLine("Enter the Customer Id of the Customer");
					int C_Id = Convert.ToInt32(Console.ReadLine());
					Console.WriteLine("\n");
					
					Console.WriteLine("Enter the Age of the Customer");
					int age = Convert.ToInt32(Console.ReadLine());
					Console.WriteLine("\n");
					
					Console.WriteLine("Enter the Address of the Customer");
					string address = Console.ReadLine();
					Console.WriteLine("\n");
					
					customerList.Add(new Customer(fName,lName,C_Id,age,address));
					Console.WriteLine("Customer Added Successfully !!!");
					Console.WriteLine("\n");
				
					goto Start;
				
				case 2:
					
					Console.WriteLine("CustomerID \t First Name \t Last Name \t Age \t Address");
					Console.WriteLine("----------------------------------------------------------------");
					foreach (var customer in customerList)
					{
						Console.WriteLine(customer.CustomerID +"\t\t"+customer.FirstName + "\t\t" + customer. LastName+ "\t\t" + customer.Age+ "\t" + customer.Address);
						Console.WriteLine("\n");
					}
					Console.WriteLine("\n");
					goto Start;
				
				case 3:
				
				Console.WriteLine("Enter the Customer Id of the Customer you want to remove.");
					int id = Convert.ToInt32(Console.ReadLine());
					Console.WriteLine("\n");
					
					for(int i=0;i<customerList.Count;i++)
					{
						var element = customerList[i];
						if(id==element.CustomerID)
						{
							customerList.Remove(element);
						}
					}
					
					Console.WriteLine("Customer Removed...");
					Console.WriteLine("\n");
					goto Start;
					
				case 4:
					Console.WriteLine("Enter the Customer Id of the Customer to be searched.");
					int id1=Convert.ToInt32(Console.ReadLine());
					
					foreach (var customer in customerList)
					{
						if(id1==customer.CustomerID)
						{
							var element = customer;
							if(customerList.Contains(element))
							{
								Console.WriteLine(customer.FirstName+"is Found.");
								Console.WriteLine("\n");
							}
					
						}
					}
					
						goto Start;
						
				case 5:
				
					Console.WriteLine("Enter 1 :: Sort By Cid || 2 :: Sort By FirstName || 3 :: Sort By LastName ||4 :: Sort By Age");
					Customer.val =Convert.ToInt32(Console.ReadLine());
					
					customerList.Sort();
					
					//customerList.Sort();
					Console.WriteLine("CustomerID \t First Name \t Last Name \t Age \t Address");
					Console.WriteLine("----------------------------------------------------------------");
					
					foreach (var customer in customerList)
					{
						Console.WriteLine(customer.CustomerID +"\t\t"+customer.FirstName + "\t\t" + customer. LastName+ "\t\t" + customer.Age+ "\t" + customer.Address);
						Console.WriteLine("\n");
					}
					goto Start;
				
			}
		}
	}
	

	