using System;
using System.Linq;

public class Program
{
	static void Main(string[] args)
	{
		Console.WriteLine("Enter a string");
		string str = Console.ReadLine();
		
		string temp = new String(str.Distinct().ToArray());
		
		int i=0;
		int[] count = Enumerable.Repeat(0, temp.Length).ToArray();
		
		foreach(char c in str)
		{
			i =temp.IndexOf(c);
			if(temp.Contains(c))
			{
				count[i]++;
			}
			
		}
		
		//To Print the characters
		foreach(char c in temp)
		{
			i =temp.IndexOf(c);
			Console.WriteLine(c +"\t\t"+ count[i]);
		} 
}
}