using System ;

class program
{
	static void Main(string[] args)
	{
		string s1= "Hello " ;
		string s2 = " World";
		string s3 = @"Come visit us at http://www.LibertyAssociates.com " ;
		string s4 = s1+s2;
		string s5 = s2.ToLower();
		string s6 = string.Copy(s3);
		
		
		Console.WriteLine("String1: "+ s1);
		Console.WriteLine("String2: "+ s2);
		Console.WriteLine("String3: "+ s3);
		Console.WriteLine("String4: "+ s4);
		Console.WriteLine("String5: "+ s5);
		Console.WriteLine("String6: "+ s6);
		Console.WriteLine("\n");
	
		Console.WriteLine("Length of String1: "+ s1.Length);
		Console.WriteLine("Length of String2: "+ s2.Length);
		Console.WriteLine("Length of String3: "+ s3.Length);
		Console.WriteLine("Length of String4: "+ s4.Length);
		Console.WriteLine("Length of String5: "+ s5.Length);
		Console.WriteLine("Length of String6: "+ s6.Length);
		Console.WriteLine("\n");
		
		Console.WriteLine("String1: "+ s1[2]);
		Console.WriteLine("String2: "+ s2[2]);
		Console.WriteLine("String3: "+ s3[2]);
		Console.WriteLine("String4: "+ s4[2]);
		Console.WriteLine("String5: "+ s5[2]);
		Console.WriteLine("String6: "+ s6[2]);
		Console.WriteLine("\n");
		
		bool contains = (s1.Contains("H") || s1.Contains("h"));
		Console.WriteLine("Does '{0}' contain 'h' ? {1}", s1,contains);
		
		// // reversing a string
		// for (int i = 0; i < s1.Length; i++)
		// {
    		// Console.Write(s1[s1.Length - i - 1]);
			
		// }
			Console.WriteLine("\n");
		for(int j=s2.Length-1; j>=0;j--)
		{
			Console.Write(s2[j]);
		}
		
	
	}

}