using System;
class Dog
{
	
	private int weight;
	private string name;
	
	public void get()
	{
		Console.WriteLine("Enter the name of the Dog");
		name = Console.ReadLine();
		Console.WriteLine("Enter the weight of the Dog");
		weight = Convert.ToInt32(Console.ReadLine());
	}
	
	public void display()
	{
		Console.WriteLine("Name:{0}", name);
		Console.WriteLine("Weight:{0} Pounds", weight);
	}
	 static void Main(string[] args)
	 {
		 Dog ob1 = new Dog();
		 ob1.get(); 
		 ob1.display();
		 
	 }
}