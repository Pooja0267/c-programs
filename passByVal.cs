using System;
class Program
{
	static int Add(int x)
	{
		Console.WriteLine("I am in called function 1 & my value is: "+x);
		return x+=20 ;
	}
	
	static int Sub(int y)
	{
		Console.WriteLine("I am in called function 2 & my value is: "+y);
		return y-5 ;
	}
	
	static void Main(string[] args)
	{
		
		int a = 10;
		Console.WriteLine("I am in Main Method & my value is :"+a);
		Console.WriteLine("Value returned from called Function 1: "+ Add(a));
		Console.WriteLine("Value returned from called Function 2: "+ Sub(a));
		Console.WriteLine("I am Back to Main Method & my value is :"+a);
		
		
		
	}
}