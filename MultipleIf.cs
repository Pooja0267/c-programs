using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class Program
{
	static void Main( )
	{
		Console.WriteLine("Enter an Integer");
		int a= Convert.ToInt32(Console.ReadLine());
		
		if(a==0)
		Console.WriteLine("The Value entered is Zero.");
		
		if(a%2==0)
		Console.WriteLine("The Value entered is an Even Number.");
		
		else
		Console.WriteLine("The Value entered is an Odd Number.");
		
		if(a%10==0)
		Console.WriteLine("The Value entered is multiple of Ten.");
		
		if(a>100)
		Console.WriteLine("The Value entered is Too Large.");
	}

}

