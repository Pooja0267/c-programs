using System;

class Program
{
	
	static void Main( )
	{
		Console.WriteLine("Enter a number between 10 and 10000");
		string s = Console.ReadLine();
		int n = Convert.ToInt32(s);

		int temp;
		int rem=0;
		int rev=0;
		if(n>10 && n<10000)
		{
			temp = n;
			while(n>0)
			{
				rem = n%10;
				rev = rev*10 + rem ;
				n = n/10;	
			}
			
			if(temp == rev)
			{
				Console.WriteLine("Yes");
				
			}
			else
			{
				Console.WriteLine("No");
				
			}
			
		}
		else
		{
			Console.WriteLine("Invalid Number");
			//return -1 ;
		}
	}

}

