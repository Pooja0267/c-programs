using System;
class Chess
{
	static void FillArray(out string[,] arr)
    {
        // Initialize the array:
        arr = new string[8,8];
	
		for(int i=0;i<=7;i++)
		{
			for(int j=0;j<=7;j++)
			{
				if(i+j%2==0)
				{
					arr[i,j]="Black";
				}
				else
				{
					arr[i,j]="White";
				}
			}
		}
	
    }
	

	static void Main(string[] args)
	{
		string[,] chessBoard ;
	
		FillArray(out chessBoard);
		
		Console.WriteLine("Enter two values between 0-8");
		int x= Convert.ToInt32(Console.ReadLine());
		int y= Convert.ToInt32(Console.ReadLine());
		Console.WriteLine( chessBoard[x,y]);
		
		
	
	
	
	
}
}