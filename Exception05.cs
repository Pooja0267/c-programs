/*Create a simple array of three integers. Ask the user which array element she wants to see. Output the integer that the user asked for (remember that the
user probably won’t ask for a zero-based index). Provide a way for the user to indicate whether she wants another integer, or to end the program. Provide a handler
that deals with invalid input. */

using System;

public class MyCustomException : System.Exception
{
	public MyCustomException(string Message):base(Message)
	{
		
	}
}

class Program
{
	static void Main(string[] args)
	{
		int count=0;
		try
		{
			Console.WriteLine("Enter the size of the array");
			int size = Convert.ToInt32(Console.ReadLine());
			
			int[] myArray = new int[size];
			
			for(int i=0;i<size;i++)
			{
				myArray[i]= Convert.ToInt32(Console.ReadLine());
			}
			
			Console.WriteLine("Enter the Array Element you want to see");
			int choice = Convert.ToInt32(Console.ReadLine());
			
			for(int i=0;i<size;i++)
			{
				if(myArray[i]== choice)
				{
					Console.WriteLine("Element Found!!!"+ myArray[i]);
					count=1;
				}
				
			}
		
			if(count!=1)
			{
				MyCustomException ex = new MyCustomException("Element not Found");
				throw ex;
			}
			
		
		}
		catch (IndexOutOfRangeException e)  
        {
            Console.WriteLine(e.Message)
		}
		
		catch(MyCustomException ex)
		{
			Console.WriteLine("Exception Caught");
			Console.WriteLine(ex.Message);
		}
	}
}