//Take a string as input , and return the first non repeating character of that string. If no such character is present return “-1”.
using System;
class Program
{
	static void Main(string[] args)
	{
		Console.WriteLine("Enter a string");
		string str = Console.ReadLine();
		int value= 0;
		for(int i=0;i<str.Length;i++)
		{
			int count= 0;
			for(int j=0;j<str.Length;j++)
			{
				if(str[i]==str[j])
				{
					count+=1;
				}
			}
			if(count==1)
				{
					value=i;
					break;
				}
		}
		Console.WriteLine("The first Non-Repeatative character is: {0}", str[value]);
	}
}