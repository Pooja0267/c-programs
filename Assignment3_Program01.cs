/*Write a method to compute all permutations of a string.

example:
input: abc
output : “abc”, “bac”, “bca”,“acb”, “cab”, “cba” */

using System.Collections.Generic;
using System;
//using System.Generic;
using System.Linq;
using System.Text;
namespace Permutation
{
    class Program
    {
    static void Main(string[] args)
        {
            Permutation("abc");
        }
		
		 public static void Permutation(string input)
        {
            RecPermutation("", input);
        }
    private static void RecPermutation(string soFar, string input)
        {
    if (string.IsNullOrEmpty(input))
		{
            Console.WriteLine(soFar);
			return;
        }
    else
        {
			for (int i = 0; i < input.Length; i++)
            {
                    
				string remaining = input.Substring(0, i) + input.Substring(i + 1);
                RecPermutation(soFar + input[i], remaining);
            }
            
        }
    }
}
}