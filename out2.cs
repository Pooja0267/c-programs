using System;
class OutExample
{
    static void Method(out int i)
    {
		//Console.WriteLine(i);
        i = 1;
    }
	
	static void Method2(ref int i)
    {
		Console.WriteLine(i);
        i+= 44;
    }
    static void Main(string[] args)
    {
        int value;
        Method(out value);
        // value is now 1
		Console.WriteLine(value);
		Method2(ref value);
		Console.WriteLine(value);
		
    }
}