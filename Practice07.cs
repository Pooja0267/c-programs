//Create a method which accepts a String and replaces all the consonants in the String with the next alphabet.
//Method Name- alterString
//Method Description-- Replace consonants
//Argument- String
//Return Type- String
//Logic- Return the String replacing all the consonants with the next character.
// For Example :JAVA should be changed as KAWA

using System;
class Program
{
	public void alterString(string s)
	{
		int [] arr = new int[50];
		string output  = String.Empty;
		for(int i=0; i<s.Length; i++)
		{
			if(s[i]=='a' || s[i]=='A' || s[i]=='e' || s[i]=='E' || s[i]=='i' || s[i]=='I' || s[i]=='o' || s[i]=='O' || s[i]=='u' ||s[i]=='U')
				{
					arr[i] = (int)s[i];
				}
			else
			{
				arr[i] = (int)s[i]+1;
			}
			//Console.WriteLine((int)s[i]);
			output = output +(char)arr[i];
		}
		
		Console.WriteLine(output);
	}
}

class Tester
{
	static void Main(string[] args)
	{
		Console.WriteLine("Enter a string");
		string input = Console.ReadLine();
		Program obj = new Program();
		obj.alterString(input);
		
		
	}
}
