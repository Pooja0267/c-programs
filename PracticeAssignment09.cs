// Write a program to find intersection of two sorted array

using System;

class Program
{
	static void Main(string[] args)
	{
		//Array 1
		Console.WriteLine("Enter the size of first array");
		int size1= Convert.ToInt32(Console.ReadLine());
		
		int[] array1 = new int[size1];
		
		Console.WriteLine("Enter the elements of first array");
		for(int i=0;i<size1;i++)
		{
			array1[i] = Convert.ToInt32(Console.ReadLine());
		}
		
		//Array 2
		Console.WriteLine("Enter the size of second array");
		int size2= Convert.ToInt32(Console.ReadLine());
		
		int[] array2 = new int[size2];
		
		Console.WriteLine("Enter the elements of second array");
		for(int j=0;j<size2;j++)
		{
			array2[j] = Convert.ToInt32(Console.ReadLine());
		}
		
		Console.WriteLine("The common elements are:: ");
		for(int m =0;m<size1; m++)
		{
			for(int n=0; n<size2; n++)
			{
				if(array1[m]==array2[n])
				{
					Console.Write(array1[m]);
					Console.Write("\t");
				}
			}
		}
	}
}
