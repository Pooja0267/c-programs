using System;

public class Program
{
	public int Add(int a, int b)
	{
		return a+b ;
	}
	
	public int Sub(int a, int b)
	{
		return b-a ;
	}
	
	public int Mul(int a, int b)
	{
		return a*b ;
	}
	
	public int Div(int a, int b)
	{
		return b/a ;
	}

	static void Main(string[] args)
	{
		int x= 10;
		int y = 20;
		Program ob = new Program();
		int sum = ob.Add(x,y);
		int diff = ob.Sub(x,y);
		int product = ob.Mul(x,y);
		int quotient = ob.Div(x,y);
		
		Console.WriteLine("Sum={0} ",sum);
		Console.WriteLine("Difference={0}",diff);
		Console.WriteLine("Product={0}",product);
		Console.WriteLine("Quotient= {0}",quotient);
		
		
		
		Console.WriteLine("Enter first Number");
		int m = Convert.ToInt32(Console.ReadLine());
		Console.WriteLine("Enter second Number");
		int n = Convert.ToInt32(Console.ReadLine());
		
		Program ob2 = new Program();
		int sum2 = ob2.Add(m,n);
		int diff2 = ob2.Sub(m,n);
		int product2 = ob2.Mul(m,n);
		int quotient2 = ob2.Div(m,n);
		
		Console.WriteLine("Sum={0} ",sum2);
		Console.WriteLine("Difference={0}",diff2);
		Console.WriteLine("Product={0}",product2);
		Console.WriteLine("Quotient= {0}",quotient2);
	}

}

