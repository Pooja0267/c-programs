using System;

class Program{
	static void Main(string[] args)
	{
		Console.WriteLine("Enter a Name.");
		string name = Console.ReadLine().ToLower();
		
		string n1="";
		string n2 = "";
		string sortedVowels= "";
		string sortedConsonants= "";
		
		for(int i =0; i<name.Length; i++)
		{
				if(name[i]=='a' || name[i]=='A' || name[i]=='e' || name[i]=='E' || name[i]=='i' || name[i]=='I' || name[i]=='o' || name[i]=='O' || name[i]=='u' ||name[i]=='U')
				{
					n1 = n1+name[i];
					
					Char[] vowels = n1.ToCharArray();
					Array.Sort(vowels);
					sortedVowels = new string(vowels);
				}
				else
				{
					n2 = n2+name[i];
					
					Char[] consonants = n2.ToCharArray();
					Array.Sort(consonants);
					sortedConsonants = new string (consonants);
					
				}
		}
		
		Console.WriteLine("Sorted Name:{0}" , sortedVowels+sortedConsonants);
	}
}