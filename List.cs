//Generic Collection: List
using System;
using System.Collections;
using System.Collections.Generic;

class Program
{
    static void Main(string[] args)
    {
		//List 1 , Integer type
		List<int> list = new List<int>();
		list.Add(2);
		list.Add(3);
		list.Add(5);
		list.Add(7);
		list.Add(8);
		list.Add(4);
		list.Add(6);
		list.Add(9);
		//List<int> list = new List<int>(new int[]{ 2, 3, 7, });
		
		foreach (int prime in list)
		{
			System.Console.WriteLine(prime);
		}
		
		Console.WriteLine("Count= {0}",list.Count); 
		list.Clear();
		Console.WriteLine("Count= {0}",list.Count); 
		
		int index = list.IndexOf(4); 
		Console.WriteLine("Index Of 4: {0}",index);
		
		//List 2 string type
		List<string> colors = new List<string>();
		colors.Add("Red");
		colors.Add("Blue");
		colors.Add("Green");
		colors.Add("Black");
		colors.Add("White");
		colors.Add("Yellow");
		
		 foreach (string color in colors)
		{
			Console.WriteLine(color);
		}
		 Console.WriteLine("Count= {0}",colors.Count); 
		 colors.Insert(3, "Violet");
		 foreach (string color in colors)
		{
			Console.WriteLine(color);
		}
		Console.WriteLine("Count= {0}",colors.Count); 
		colors.Remove("Blue");
		Console.WriteLine("Count= {0}",colors.Count); 
		 
		foreach (string color in colors)
		{
			Console.WriteLine(color);
		}
		 
		 foreach (string color in colors)
		{
		  if (colors.Contains(color))
		  {
			Console.WriteLine("{0} color exist in the list",color);
		  }
		  else
		  {
			Console.WriteLine("{0} color does not exist in the list",color);
		  }
		}
    }
}