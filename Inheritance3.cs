using System;
public class Telephone
{
	protected string phonetype;
	public void Ring()
	{
		Console.WriteLine("Ringing the:{0}", phonetype );
	}
}

class ElectronicTelephone : Telephone
{
	ElectronicTelephone()
	{
		phonetype= "Digital" ;
		Console.WriteLine("Phone Type: {0}", phonetype);
	}
	public void Run()
	{
		base.Ring();
		Console.WriteLine("Ringing the ElectronicTelephone.");
	}
	
	static void Main(string[] args)
	{
		ElectronicTelephone ob = new ElectronicTelephone();
		ob.Run();
	}

}