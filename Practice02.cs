//Create a class with a method to find the difference between the sum of the squares and the square of the sum of the first n natural numbers.
using System;
public class Program
{
	public int calculateDifference(int n)
	{
		//To find the sum of squares
		int a=0;
		int b= 0;
		int c=0;
		for(int i=1;i<=n;i++)
		{
			a=a+ i*i;
		}

		for(int j=1;j<=n;j++)
		{
			 b=b+j;
		}
		
		c= b*b;
		
		return a-c;
		
		
		
	}
}
public class Tester
{
	static void Main(string[] args)
	{
		Console.WriteLine("Enter the number to set the Range");
		int num = Convert.ToInt32(Console.ReadLine());
		
		Program obj = new Program();
		int diff = obj.calculateDifference(num);
		Console.WriteLine("Difference= {0}", diff);
		
	}
}