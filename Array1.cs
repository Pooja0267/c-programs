using System;

class Program
{
	static void Main(string[] args)
	{
		int[] myIntArray = new int[10];
		
		for (int i = 0; i < myIntArray.Length; i++)
		{
			myIntArray[i] = 2 * (i + 1);
		}
		for (int i = 0; i < myIntArray.Length; i++)
		{
			Console.WriteLine("Value in index {0} is {1}.", i, myIntArray[i]);
		}
	}
}