using System;
public class DigitalPhone
{
	public virtual void VoiceMail()
	{
		Console.WriteLine("You have a message. If you use a DigitalPhone then Press Play to retrieve.");
	}
}
public class DigitalCellPhone : DigitalPhone
{
	public override void VoiceMail()
	{
		Console.WriteLine("You have a message. If you use a DigitalCellPhone then Call to retrieve.");
	}
	
	public static void Main(string[] args)
	{
		 DigitalPhone ob = new DigitalPhone();
		 ob. VoiceMail();
		 
		 DigitalCellPhone ob2 = new DigitalCellPhone();
		 ob2. VoiceMail();
	}
}