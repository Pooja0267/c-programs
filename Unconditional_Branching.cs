using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class Program
{
	static void Main( )
	{
		Console.WriteLine("In Main Method!...");
		Method1( );
		Method3();
		Console.WriteLine("Back in Main( ).");
	}

	static void Method1( )
	{
		Console.WriteLine("Greetings from Method-1!");
		Method2();
	}
	
	static void Method2( )
	{
		Console.WriteLine("Greetings from Sub-Method-2!");
	}
	
	static void Method3( )
	{
		Console.WriteLine("Greetings from Method-3!");
	}

}

