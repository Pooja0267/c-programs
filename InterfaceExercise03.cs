//Interface ex-03
using System;
interface IConvertible
{
	string ConvertToCSharp(string str1);
	string ConvertToVB(string str2);
}

interface ICodeChecker: IConvertible
{
	bool CodeCheckSyntax(string str3, string str4);
}

class ProgramHelper: ICodeChecker
{
	public string ConvertToCSharp(string s1)
	{
		Console.WriteLine("Your code: "+ s1 + " will be converted to C# here.");
		return "Done to C#";
	}
	
	
	public string ConvertToVB(string s2)
	{
		Console.WriteLine("Your code: "+ s2 + " will be converted to VB here.");
		return "Done to VB";
	}
	
	public bool CodeCheckSyntax(string s3, string s4)
	{
		if(s4== "C#")
		return true;
	
		else if(s4=="VB")
		return true;
	
		else
		return false;
	}
	
	
	static void Main(string[] args)
	{
		ProgramHelper obj = new ProgramHelper();
		
		Console.WriteLine("Enter code to convert to CSharp");
		string codeCsharp = obj.ConvertToCSharp(Console.ReadLine());
		Console.WriteLine(codeCsharp);
		
		bool response1 = obj.CodeCheckSyntax(codeCsharp,"C#");
		Console.WriteLine(response1);
		
		Console.WriteLine("Enter code to convert to VB");
		string codeVB = obj.ConvertToVB(Console.ReadLine());
		Console.WriteLine(codeVB);
		
		bool response2 = obj.CodeCheckSyntax(codeVB,"VB");
		Console.WriteLine(response2);
	}
}