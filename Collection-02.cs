/*Replace the array in Exercise 14-1 with a List. Sort the animals by size.
You can simplify by just calling ToString() before and after the sort. 
Remember that you’ll need to implement IComparable. */

using System;
using System.Collections;
using System.Collections.Generic;

abstract class Animal : IComparable
{
	//private int weight;
	//private string name;
	
	/*public int weight
	{
		get
		{
			return weight;
		}
		set(int value)
		{
			weight = value;
		}		
	}
	
	public string name
	{
		get
		{
			return name;
		}
		set(string value)
		{
			name = value;
		}		
	}*/
	
	abstract public void Speak();
	abstract public void Move();
	abstract public void ToString1();
}

class Cat : Animal
{
	string n=string.Empty;
	int w = 0;
	
	public override void Speak()
	{
		Console.WriteLine("A cat purrs.");
	}
	public override void Move()
	{
		Console.WriteLine("A cat Jumps.");
	}
	public override void ToString1()
	{
		Console.WriteLine("Enter the cat's name");
		n = Console.ReadLine();
		
		Console.WriteLine("Enter the cat's weight");
		w = Convert.ToInt32(Console.ReadLine());
		
	}
}

class Dog: Animal
{
	string n= string.Empty;
	int w= 0;
	
	public override void Speak()
	{
		Console.WriteLine("A dog barks.");
	}
	public override void Move()
	{
		Console.WriteLine("A dog runs.");
	}
	public override void ToString1()
	{
		Console.WriteLine("Enter the dog's name");
		n = Console.ReadLine();
		
		Console.WriteLine("Enter the dog's weight");
		w = Convert.ToInt32(Console.ReadLine());
	}
}
class Program
{
	static void Main(string[] args)
	{
		List<Cat> cats = new Cat();
		cats.Add(cat1);
		cats.Add(cat2);
		cats.Add(cat3);
		cats.Add(cat4);
		foreach(catty in cats)
		{
			catty.ToString1();
			catty.Speak();
			catty.Move();
		}
		
	}
}