/* Write a function to remove duplicate characters from String.
Example:
input:  abcdaghb
output: abcdgh */

using System;
using System.Linq;
public class Program
{
	public static void Main(string[] args)
	{
		Console.WriteLine("Enter a name");
		string name= Console.ReadLine();
		string str = new String(name.Distinct().ToArray());
		Console.Write(str);
		
	}
}