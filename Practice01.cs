//Calculate the sum of first n natural numbers which are divisible by 3 or 5.
using System;
public class Tester
{
	public int calculateSum(int n)
	{
		int s = 0;
		for(int i=1;i<=n;i++)
		{
			if(i%3==0 || i%5==0)
			{
				s= s+i;
				Console.WriteLine(i);
			}
			
		}
		return s;	
	}
}
class Program
{
	static void Main(string[] args)
	{
		Tester obj =new Tester(); 
		Console.WriteLine("Enter a number to set the range");
		int num = Convert.ToInt32(Console.ReadLine());
		int sum = obj.calculateSum(num);
		Console.WriteLine("Sum={0}", sum);
	}
}
