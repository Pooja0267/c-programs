//Program 2: Write a function to test if any input String is null or Zero Length.
using System;

public class Program
{
	static void Main(String[] args)
	{
		
		string str1 = null ;
		Console.WriteLine("Enter a String");
		string str2 = Console.ReadLine();
		//string str3 = str1 ?? str2;
		
		//Checking String 1
		
		if (String.IsNullOrEmpty(str1)) 
        Console.WriteLine("String 1 is null or empty");
   	
		if(str1== null)
		{
			Console.WriteLine("String 1 is Null");
		}
		else
		{
			Console.WriteLine("String 1  is not Null");
		}
		
		/*if(str1.Length==0)
		{
			Console.WriteLine("String 1 is Zero Length");
		}
		else
		{
			Console.WriteLine("String 1 is Not Zero Length");
		}  */
		
		//Checking String 2
		if(str2== null)
		{
			Console.WriteLine("String 2 is Null");
		}
		else
		{
			Console.WriteLine("String 2 is not Null");
		}
		
		
		
		if(str2.Length==0)
		{
			Console.WriteLine("String 2 is Zero Length");
		}
		else
		{
			Console.WriteLine("String 2 is Not Zero Length");
		}
	}
}